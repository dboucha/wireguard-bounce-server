```
Date:        2020-12-15
Reply-to:    Nathan Myers <ncm@cantrip.org>
```
# A Wireguard Bounce Server

I find plenty of tutorials online for setting up the most
basic Wireguard apparatus, but I found practically nothing
about what I actually needed:
Like most peoples', my machines are stuck behind NATs.
To connect between NATted hosts, you need control of a host
that is not, to keep up on what external addresses the NATs
are presenting.
It seems like the VPN services ought to provide bounce
service, but I haven't found any that say they do
(except Tailscale?)
The docs for Wireguard mention bounce servers, but say nothing
about how to set one up.

Here is what I worked out. There's nothing earthshaking,
just details.  Suggested improvements welcome.

## Starting Out

I assume you have already set up a pair of Wireguard hosts
with a private network of your own, but both hosts are
behind NAT and can't see one another.  

Thus, you already have a `/etc/wireguard/wg0.conf` on one host:

~~~
  [Interface]
  # Name = home
  Address = 10.123.321.2
  PrivateKey = IN0OpWrUc3zy5sSCNSvP3f72Xrw7NccZhoyAcaiFpXE=

  # [Peer]    # If only...
  # Name = office
  # AllowedIPs = 10.123.321.3/32
  # PublicKey = EXHDoGyCLT+/CgiLTc4rnR7iTqHeEANDqZwWP86nnkU=
~~~

And another at (say) the office:

~~~
  [Interface]
  # Name = office
  Address = 10.123.321.3
  PrivateKey = INjuNbG8V6u+P59m2bvmgwLmivXVrJgisIamA7Y6M14=

  # [Peer]    # If only...
  # Name = home
  # AllowedIPs = 10.123.321.2/32
  # PublicKey = zUrGRQcIWEXgka2MflmkbBanUmEszy/KMr7K5f35z0I=
~~~

All we need is a machine on the public internet for them
to talk through.
A Bounce Server.
It hardly needs to be anything.
It just needs to take in UDP packets on a single port, and send
out UDP packets of its own.

## Amazon Lightsail, or whatever

Amazon offers a cheap service called "Lightsail" for US$3.50/month.
It won't be as secure other alternatives, because Amazon techs
and motivated hackers can peek at files you put there (yes, really,
all of them! including your secret keys)
but it is quick and easy to set up, and good practice.
If you have someplace to put more secure equipment on the open net,
that would be better.
A Pi would do fine, if it runs only your code.
You can set up a Lightsail server first, for practice, and later
change to someplace else.
(Amazon gives you the first 30 days free,
so you can delete it once you have it working, if you like, at no
charge.)
Most of the below applies to any such server you set up.

So, I will assume you have gone to `https://signin.aws.amazon.com`,
made an account, and set up your "multi-factor authentication"
(maybe using the `FreeOTP` app on your phone).
Then, you made an "OS Only", maybe the "Ubuntu 20.04 LTS" image;
and named it `bounce`.
Whichever OS you choose, it should be a pretty recent release,
with wireguard already compiled in.
E.g., for Debian 10 you would need to pull a new kernel from the
backports repository (which you totally can).

You will have "Assigned" it a "static IP" address.
Amazon handed you, say, `99.99.99.111`, and offered to let
you SSH into it.
The first time you do, you will use *their* key, just the one time.
But first... on your own machine, say:

~~~
  $ ssh-keygen -t ed25519 -f ~/.ssh/bounce
  Generating public/private ed25519 key pair.
  ...
  The key fingerprint is:
  SHA256:QFWdoRlQj8bcN9K2HpQS3EZXT5upmxAXS+DtAXQDCz4 you@home
  ...
~~~

Now, start and log into your new Amazon image,
`admin@99.99.99.111` using their key, or even just through
their dashboard web page button, "`Connect using SSH`".

(They will present a shell prompt with some internal IP
address in it; but I will write "bounce" here, instead.)
The first thing to do is copy in your new key fingerprint,
from above:

~~~
  bounce$ cat >>.ssh/authorized_keys
  SHA256:QFWdoRlQj8bcN9K2HpQS3EZXT5upmxAXS+DtAXQDCz4 you@home
  ^D
  bounce$
~~~

(If your image has an older OpenSSH server, you might need to put
in a copy of the actual public key, not just its fingerprint.)
Log out, and then back in using the new key:

~~~
  $ ssh -i ~/.ssh/bounce admin@99.99.99.111
~~~

Then:

~~~
  bounce# apt update
  bounce# apt install wireguard wireguard-dkms wireguard-tools nftables
  bounce# apt upgrade
  bounce# apt clean
  bounce# echo wireguard >> /etc/modules-load.d/modules.conf
  bounce# echo 'net.ipv4.ip_forward=1'  >> /etc/sysctl.conf
  bounce# reboot
~~~

Next, you will need a `/etc/wireguard/wg0.conf` file like on
your other hosts.
First, generate another public/private keypair for it.

~~~
  $ wg keygen
  8MOsM2vRrnD1qkJaKB2eDp9QKyMa0UMBzlBq+Ej7JmQ=
  $ echo 8MOsM2vRrnD1qkJaKB2eDp9QKyMa0UMBzlBq+Ej7JmQ= | wg pubkey
  kv/GBU+t6lygj4Hi/vOgrrSo5SXP8LGkZeuz8DNdOTk=
~~~

Add a Peer stanza to the `/etc/wireguard/wg0.conf` on *each* of
your client hosts, home and office, using the second, public,
key above, Amazon's public IP address, and your choice of
port number in place of `54321`:

~~~
  [Peer]
  # Name = bounce
  AllowedIPs = 10.123.321.0/24
  PublicKey = kv/GBU+t6lygj4Hi/vOgrrSo5SXP8LGkZeuz8DNdOTk=
  Endpoint = 99.99.99.111:54321
  PersistentKeepalive = 25
~~~

(You don't *need* to do both client hosts, immediately.
You can fix up your office host later.)
Notice the "`.0/24`" above -- you are telling it to
route traffic to any hosts on your private network.
Then, make a new configuration for your bounce server,

~~~
   $ ssh -i ~/.ssh/bounce admin@99.99.99.111
   bounce$ sudo bash
   bounce# vi /etc/wireguard/wg0.conf
~~~

This one gets a `ListenPort` with your choice of port
from above, and the *private* key just generated above;
and actual peers with *their* public keys:

~~~
  [Interface]
  # Name = bounce
  Address = 10.123.321.1
  ListenPort = 54321
  PrivateKey = 8MOsM2vRrnD1qkJaKB2eDp9QKyMa0UMBzlBq+Ej7JmQ=

  [Peer]
  # Name = home
  AllowedIPs = 10.123.321.2/32
  PublicKey = zUrGRQcIWEXgka2MflmkbBanUmEszy/KMr7K5f35z0I=
  # PersistentKeepalive = 300

  [Peer]
  # Name = office
  AllowedIPs = 10.123.321.3/32
  PublicKey = EXHDoGyCLT+/CgiLTc4rnR7iTqHeEANDqZwWP86nnkU=
  # PersistentKeepalive = 300
~~~

Turn on wireguard service:

~~~
  bounce# systemctl enable wg-quick@wg0.service
  bounce# systemctl daemon-reload
  bounce# systemctl start wg-quick@wg0
  bounce# systemctl status wg-quick@wg0
~~~

Systemd integration is great when it works at all.
Restart wireguard on your other hosts:

~~~
  # wg-quick down /etc/wireguard/wg0.conf
  # wg-quick up /etc/wireguard/wg0.conf
  # wg show
~~~

(Or maybe just "`systemctl reload wg-quick@wg0`";
why should your server get all the `systemd` fun?)
They should be able see your bounce server *and* 
one another, now.

~~~
  $ ping 10.123.321.1 # your bounce server
  $ ping 10.123.321.3 # your other host!
~~~

Now, go to the Amazon dashboard for your image,
click on the 3 dots, and go to the "`Snapshots`" tab.
Make a snapshot, and name it maybe "bounce-works".

But you're not done.
Next, you will secure your bounce server.

## Securing Your Server

First, delete Amazon's keys, all but the last line,
from `.ssh/authorized_keys`:

~~~
  bounce$ vi .ssh/authorized_keys
~~~

Then, turn off password logins; edit the
`sshd` config file:

~~~
  bounce$ sudo bash
  bounce# vi /etc/ssh/sshd_config
~~~

so it has

~~~
  PasswordAuthentication: no
  ChallengeResponseAuthentication: no
~~~

and add a line with your private VPN address,
alongside their public one:

~~~
  ListenAddress 0.0.0.0  # keep the default, too, for now.
  ListenAddress 10.123.321.1:2222
~~~

then reload it, and log out:

~~~
  bounce# systemctl reload ssh
  bounce# exit
  bounce$ exit
~~~

SSH is listening at your VPN address.
Now, add an entry to your `ssh` config
(notice the `Hostname` entry has your VPN address,
not Amazon's public one, and the `IdentityFile`
line points to your key you made):

~~~
  $ cat >> ~/.ssh/config
  Host bounce
    Hostname 10.123.321.1
    User admin
    IdentityFile /home/yourname/.ssh/bounce
    Port 2222
  ^D
  $
~~~

Now, you can log into your bounce server, if
you ever need to again, using just:

~~~
  $ ssh bounce
  bounce$
~~~

*NOTE:* Systemd has a nasty habit of delaying logins for
25 - 100 seconds while it waits for some service you
never asked for to time out and fail, before it lets you
have a prompt.
If you don't get a prompt right away, *DON'T PANIC*.
Get a cuppa and see if it comes through in the end.
If this happens, you might need to delete `gnupg` packages
or something; check "`journalctl -ef`" output on one terminal
while you log in on another one, and see what the hang-up is.

Next, if you used the Ubuntu image, clean out the nasty `snap`
and its packages and whole package system.
That frees *300MB* from your image:

~~~
  bounce$ sudo bash
  bounce# snap list
  bounce# snap remove --purge lxd
  # ... (and any others, then)
  bounce# snap remove --purge core18
  bounce# snap remove --purge snapd
  bounce# dpkg --purge snapd
  bounce# rm -rf /var/cache/snapd/
  bounce# rm -rf ~/snap/
  bounce# apt-mark hold snapd  # and don't come back
~~~

Next, edit `/etc/nftables.conf`

~~~
  bounce# vi /etc/nftables.conf
~~~

and add lines so it has:

~~~
  chain input {
    type filter hook input priority 0;
    tcp dport { 22 } accept;
    udp dport { 54321 } accept;  # the right number?
    tcp dport { 2222 } accept;
    ip protocol icmp accept;
    policy drop;
    # policy accept  # change to this line when using apt
  }
  chain forward {
    type filter hook forward priority 0;
    policy accept;
  }
  chain output {
    type filter hook output priority 0;
    policy accept;
  }
~~~

Make sure the numbers in the config file match your settings!
Then turn it on:

~~~
  bounce# systemctl enable nftables
  bounce# systemctl start nftables
~~~

If you mess this up, you can delete your machine
image and start over from your snapshot.
Or, if you're chicken like me, make a script:

~~~
  bounce# cat > /tmp/nfupdown
  #!/bin/bash
  systemctl start nftables
  sleep 30
  systemctl stop nftables
  ^D
  bounce# chmod +x /tmp/nfupdown
  bounce# nohup /tmp/nfupdown > /dev/null < /dev/null 2> /dev/null
~~~

and during the 30 seconds until it switches back,
you can make sure everything still works. If it
doesn't, be sure to `systemctl disable nftables`
so that if you reboot, it won't come back on.

Then, secure the machine some more:

~~~
  bounce# systemctl stop serial-getty@ttyS0
  bounce# systemctl disable serial-getty@ttyS0
  bounce# systemctl stop getty@tty1
  bounce# systemctl disable getty@tty1
~~~

Finally, go to your Amazon Lightsail dashboard page
and find your server.
Click the three-dots icon, and choose "`Manage`".
Pick the "`Networking`" tab, and go to "`IPV4 Firewall`".
Pick "`+ Add Rule`", and add a "`Custom`" rule for `UDP`
allowing all ports `1 - 65535`.

Then, using the Garbage-can icon, delete the "`SSH`" entry,
which you don't need anymore, because you are tunneling
in via your VPN, instead.

The reason to allow all UDP ports is that we made up
our own firewall rule, for `nftables`, and told it to
drop any unrecognized packets.
Now, all incoming UDP except yours will appear have no
effect, instead of getting a `REJECT` message; nobody
can tell what port your Wireguard server listens on,
or even that it's listening at all.
You still have SSH listening on public port 22, but no
outside-world packets can reach it anymore.

"Stop" and "Start" your instance, and make sure everything
still works.

Finally, pick the "`Snapshots`" tab, and make a snapshot.
They will suggest a name with a dumb number in it.
I recommend changing it to the date, `bounce-20210101`.

If you like, now, you can remove the line

~~~
  ListenAddress 0.0.0.0
~~~

from your server's `/etc/ssh/sshd_config` file, and comment
out the line

~~~
  tcp dport { 22 } accept;
~~~

from your `/etc/nftables.conf` file, because they are not
needed anymore, once you are confident about getting connected
whenever you need to.

## Closing Thoughts

Now that you have a "cloud server", you can serve two or
more VPNs from the one server, on different ports.
Or, you can route *all* your packets to the rest of the
world via your server, just like a commercial VPN.
(The easy way is to install a `SOCKS5` proxy server.)
You could put other services on it, like a `Tor` exit.
Maybe make it your `Matrix` homeserver?
(It will need more RAM for that.)

Lightsail doesn't seem to support IPv6, so I haven't put any
setup for that here, but many other services do.
IPv6 support might be needed if you want to use wireguard
on your phone; phone exchanges often support only IPv6.
But IPv6 is not supposed to have NAT. 
So, maybe you only need a bounce server just as a host with a 
stable address; but that is a legitimate use.
The difference there is that you only need the bounce server
if *neither* endpoint has a stable address.

There are lots of other hosting services, that evoke varying
degrees of fury from (now former) customers.
Be sure to check the reviews.
(BTW, I have found PCMag an unreliable reviewer.
`Hostadvice.com` is less bad.)
Some services seem to keep more of their customers than others.
I haven't seen any others quite as cheap as Amazon Lightsail,
but true $4/mo service can be found without paying for 4 years
up front.
Just make sure you get root access and a stable public IP address
in the base package; without those, it's useless.
I like best the ones that charge by the hour of server uptime;
$0.005/hr is close to $4/mo.

One possibly serious concern, only touched on above, is that
the the hosting service has full access to all your plaintext
communications, if they care, via control of your bounce server VM.
If all your WG traffic is via SSH logins and SSL connections
anyway, that might not matter:
Then, the bounce server host can see only your traffic
patterns---your metadata, who talks to whom and when---but
not the contents.
If you are tunneling terminal sessions, this could still be
a problem, because keystroke timing can be used to extract
contents;
but if you never type in passwords---because you only use
public key authentication (hint, hint) and do all your sensitive
text editing locally---that might not matter much either.

If spooks are after you, you have bigger problems than a cheap
bounce server can solve.

## References

\[1\]: <http://gitlab.com/ncmncm/wireguard-bounce-server>

\[2\]: <https://news.ycombinator.com/item?id=25447805>
